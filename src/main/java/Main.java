package main.java;


import API.Inject;
import API.Logger;
import API.Start;
import API.Stop;

public class Main {

    @Inject
    private Logger log;

    @Start
    public void start () {
        log.sendLog("Diese Nachricht wurde Ihnen präsentiert von: Logger©");
        AndereKlasse k = new AndereKlasse();
        k.machWas();
    }

    @Stop
    public void stop () {
        System.out.println("Stop called");
    }

    public static void main(String[] args) {
        Main m = new Main();
        m.start();
    }
}
