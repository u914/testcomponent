package main.java;


import API.LoggerFactory;

public class AndereKlasse {

    public void machWas () {
        try {
            LoggerFactory.createLogger().sendLog("Diese Nachricht wurde Ihnen präsentiert von: LoggerFactory©");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
